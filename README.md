# kafka

#### 介绍
Apache Kafka是分布式发布-订阅消息系统。它最初由LinkedIn公司开发，之后成为Apache项目的一部分。Kafka是一种快速、可扩展的、设计内在就是分布式的，分区的和可复制的提交日志服务。Apache Kafka与传统消息系统相比，有以下不同：
- 它被设计为一个分布式系统，易于向外扩展；
- 它同时为发布和订阅提供高吞吐量；
- 它支持多订阅者，当失败时能自动平衡消费者；
- 它将消息持久化到磁盘，因此可用于批量消费，例如ETL，以及实时应用程序。
#### 软件架构
软件架构说明
Apache kafka组件介绍
- Broker： 和RabbitMQ概念相似，Broker是一台独立的Kafka服务节点，负责消息的存储和转发。默认端口9092，集群可以包含多个Kafka Broker。生产者和消费者需要跟Broker建立连接才可以收发消息。
- Producer：消息发送方。在Kafka中，生产者不是逐条发送消息给Broker，而是批量发送的，这样可以提升消息发送速率。通过batch.size参数可以控制批次的大小
- Consumer：消息接收方，消费者。Kafka只支持pull模式，通过pull拉取信息。
- Topic：消息队列，生产者发送消息给Topic，消费者从Topic获取消息。生产者和Topic，Topic和消费者，都是多对多关系。生产者发送消息时，如果Topic不存在，会自动创Topic。
- Partition：一个Topic可以被划分成多个分区，每个分区可以对外单独提供消息服务能力，这也是分片思想的落地。Partition类似于MySQL的分库分表，为了达到横向扩展和负载均衡的目的。
- Replication: 分区副本，是Partition的冗余备份分区，当Partition不可用时，ZooKeeper会自动将Replication（Follower）分区升级为Partition（Leader）分区。
- Offset: 分区中的Record的位置标示，每一个消费者都会记录自己的消费位置（offset）。
#### ARM支持：
1. 移植指南：https://support.huaweicloud.com/prtg-apache-kunpengbds/kunpengkafka_02_0001.html
2. 部署指南：https://support.huaweicloud.com/dpmg-apache-kunpengbds/kunpengkafka_04_0001.html
3. 调优指南：https://support.huaweicloud.com/tngg-kunpengbds/kunpengkafkahdp_05_0002.html


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
