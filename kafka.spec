%global debug_package %{nil}
%global debug_package %{nil}
%define kafka_home /opt/kafka

Name: kafka
Version: 2.8.2
Release: 17
Summary: A Distributed Streaming Platform.

License: Apache-2.0
Source0: https://archive.apache.org/dist/%{name}/%{version}/%{name}-%{version}-src.tgz
Source1: https://mirrors.huaweicloud.com/gradle/gradle-6.8.1-all.zip
Source2: kafka.service
Source3: gradle-wrapper.jar
Patch0: 0001-adopt-huaweimaven.patch
Patch1: 0002-CVE-2022-41881.patch
Patch2: 0003-CVE-2023-34455.patch
Patch3: 0004-CVE-2022-42004.patch
Patch4: 0005-CVE-2016-3189.patch
Patch5: 0006-NPE-subscriptionState.patch
Patch6: 0007-fix-payload-incorrectly.patch
Patch7: 0008-Cast-SMT-allow-null.patch
Patch8: 0009-format-RocksDBConfigSetter.patch
Patch9: 0010-not-update-connection.patch
Patch10: 0011-ConfigEntry.patch
Patch11: 0012-incorrectly-LeaderElectionCommand.patch
Patch12: 0013-AlterIsr.patch
Patch13: 0014-override-toString.patch
Patch14: 0015-SessionWindows-closed-early.patch
Patch15: 0016-non-existent-URL.patch
Patch16: 0017-fix-log-clean.patch

BuildRequires: systemd java-1.8.0-openjdk-devel
Provides: kafka = %{version}
Requires: java-1.8.0-openjdk

%description
Apache Kafka is an open-source distributed event streaming platform used by thousands of companies for high-performance data pipelines, streaming analytics, data integration, and mission-critical applications.

%pre
getent group %{name} >/dev/null || groupadd -r %{name}
getent passwd %{name} >/dev/null || \
    useradd -r -g %{name} -d %{_sharedstatedir}/%{name} -s /sbin/nologin \
    -c "User for Kafka services" %{name}
exit 0

%prep
%autosetup -p1 -n %{name}-%{version}-src
cp -r $RPM_SOURCE_DIR/gradle-6.8.1-all.zip  %{_builddir}/kafka-%{version}-src/gradle/wrapper/
cp -r $RPM_SOURCE_DIR/gradle-wrapper.jar   %{_builddir}/kafka-%{version}-src/gradle/wrapper/

%build
unzip -q $RPM_SOURCE_DIR/gradle-6.8.1-all.zip
./gradle-6.8.1/bin/gradle --info
./gradlew jar releaseTarGz 


%install
mkdir -p $RPM_BUILD_ROOT%{kafka_home}/{libs,bin,config,site-docs,licenses}
cd  %{_builddir}/kafka-%{version}-src/core/build/distributions/
tar -xvf kafka_2.13-%{version}.tgz 
cp -pr  kafka_2.13-%{version} %{_builddir}
cd %{_builddir}/kafka_2.13-%{version}

cp -pr libs/* $RPM_BUILD_ROOT%{kafka_home}/libs
cp -pr bin/* $RPM_BUILD_ROOT%{kafka_home}/bin
cp -pr config/* $RPM_BUILD_ROOT%{kafka_home}/config
cp -pr site-docs/* $RPM_BUILD_ROOT%{kafka_home}/site-docs
cp -pr licenses/*   $RPM_BUILD_ROOT%{kafka_home}/licenses

%files
%defattr(-,root,root)

%{kafka_home}
%doc NOTICE
%doc LICENSE


%clean
rm -rf %{buildroot}

%changelog
* Fri Dec 08 2023 sundapeng <sundapeng_yewu@cmss.chinamobile.com> - 2.8.2-17
- log clean relative index range check of group consider empty log segment to avoid too many empty log segment left
* Fri Dec 08 2023 sundapeng <sundapeng_yewu@cmss.chinamobile.com> - 2.8.2-16
- Javadocs search sends you to a non-existent URL
* Fri Dec 08 2023 sundapeng <sundapeng_yewu@cmss.chinamobile.com> - 2.8.2-15
- SessionWindows are closed too early
* Fri Dec 08 2023 sundapeng <sundapeng_yewu@cmss.chinamobile.com> - 2.8.2-14
- override toString method to show correct value in doc
* Fri Dec 08 2023 sundapeng <sundapeng_yewu@cmss.chinamobile.com> - 2.8.2-13
- AlterIsr and LeaderAndIsr race condition
* Fri Dec 08 2023 sundapeng <sundapeng_yewu@cmss.chinamobile.com> - 2.8.2-12
- Fix noop set is incorrectly replaced with succeeded set from LeaderElectionCommand
* Fri Dec 08 2023 sundapeng <sundapeng_yewu@cmss.chinamobile.com> - 2.8.2-11
- FetchSessionCache may cause starvation for partitions when FetchResponse is full
* Fri Dec 08 2023 sundapeng <sundapeng_yewu@cmss.chinamobile.com> - 2.8.2-10
- Don't update connection idle time for muted connections
* Fri Dec 08 2023 sundapeng <sundapeng_yewu@cmss.chinamobile.com> - 2.8.2-9
- Fix the formatting of example RocksDBConfigSetter
* Fri Dec 08 2023 sundapeng <sundapeng_yewu@cmss.chinamobile.com> - 2.8.2-8
- Cast SMT should allow null value records to pass through 
* Fri Dec 08 2023 sundapeng <sundapeng_yewu@cmss.chinamobile.com> - 2.8.2-7
- Fix using random payload in ProducerPerformance incorrectly
* Mon Nov 27 2023 sundapeng <sundapeng_yewu@cmss.chinamobile.com> - 2.8.2-6
- fix NPE in subscriptionState
* Fri Nov 03 2023 sundapeng <sundapeng_yewu@cmss.chinamobile.com> - 2.8.2-5
-fix CVE-2016-3189
* Thu Sep 21 2023 sundapeng <sundapeng_yewu@cmss.chinamobile.com> - 2.8.2-4
-fix CVE-2022-42004
* Fri Sep 1 2023 sundapeng <sundapeng_yewu@cmss.chinamobile.com> - 2.8.2-3
- fix CVE-2023-34455
* Wed Aug 30 2023 sundapeng <sundapeng_yewu@cmss.chinamobile.com> - 2.8.2-2
- fix CVE-2022-41881
* Sat Sep 24 2022 xiexing <xiexing4@hisilicon.com> - 2.8.2-1
- fix CVE-2022-34917
* Thu Aug 11 2022 xiexing <xiexing4@hisilicon.com> - 2.8.1-1
- update version to fix CVE-2021-38153
* Thu Mar 25 2021 huanghaitao <huanghaitao8@huawei.com> 2.6.0 - 2
- Remove dist macros
* Sat Aug 15 2020 Hao Zhang <unioah@isrc.iscas.ac.cn>
- init
* Sat Oct 04 2020 Hao Zhang <unioah@isrc.iscas.ac.cn>
- add service
